## Instalación

* 1.- Copiar link
* 3.- ionic plugin add https://Asalmerontkd@bitbucket.org/rud0lph/allianzblink.git

## Desinstalar

1.- ionic plugin remove io-mariachi-blink

## Método

blinkplugin.blinkMethod('', funcionOk, funcionError);

## Funcion ejemplo


```
#!JavaScript

function funcionOk(response)
{
	//TODO aquí hacer lo que sea con el base 64, ejemplo:
	var datos = "data:image/jpg;base64," + response;
        document.getElementById('myImage').src = datos;
}

function funcionError(response)
{
	//TODO aquí muestra algún error como evento cancelado
}
```