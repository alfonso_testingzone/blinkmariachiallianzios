## Instalación

* 1.- Copiar liga 
* 3.- ionic plugin add https://Asalmerontkd@bitbucket.org/Asalmerontkd/allianzdocumentos.git

## Desinstalar

1.- ionic plugin remove io-mariachi-plugin-docs

## Método para documentos

allianzdocs.docFunctionLg('', funcionOk, funcionError);

## Método para identifiación

allianzdocs.docFunctionSm('', funcionOk, funcionError);

## Método para escanear tarjetas

allianzdocs.cardFunction('', funcionCard, funcionError);

## Funciones ejemplo


```
#!java Script

function funcionOk(response)
{
	//TODO aquí hacer lo que sea con el base 64, ejemplo:
	var datos = "data:image/jpg;base64," + response;
        document.getElementById('myImage').src = datos;
}

function funcionCard(response)
{
	//Obtener los datos de la tarjeta es un JSONobject
	var numTarjetaRedactado = response.redactedCardNumber;
	var numTarjata = response.cardNumber;
	var mesExpira = response.expiryMonth;
	var anioExpira = response.expiryYear;

}

function funcionError(response)
{
	//TODO aquí muestra algún error como evento cancelado
}
```