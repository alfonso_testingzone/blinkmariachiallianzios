//
//  iScanner.swift
//  ABlink_iOS
//
//  Created by Rodolfo Castillo on 3/1/17.
//  Copyright © 2017 Mariachis. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation
import QuartzCore

class iScanner: UIViewController, AVCapturePhotoCaptureDelegate {

  var pluginDelegate: AllianzDocs!
  var command: CDVInvokedUrlCommand!
    var cropperSquare: ViewFinderView!
    let stillImageOutput = AVCapturePhotoOutput()

    var shutterButton = UIButton()
    var shutterView = UIView()

    var result: String = ""
    var changeCamButton = UIButton(type: .system)

    var currentDeviceOrientation: UIDeviceOrientation = .unknown

    var imageContainer = UIImageView()

    let messageLabel = UILabel();       let rightMessageLabel = UILabel()
    let leftMessageLabel = UILabel()

    var lLMessage = UIButton();         var rLMessage = UIButton()

    let topMask = UIView();             let rightMask = UIView()
    let leftMask = UIView();            let bottomMask = UIView()
    let intermidiateView = UIView()

    private var _orientations = UIInterfaceOrientationMask.portrait
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        get { return self._orientations }
        set { self._orientations = newValue }
    }

    var OnEditingMode = false

    func setPluginDelegate(forPluggin plgn: AllianzDocs, andCommand command: CDVInvokedUrlCommand){
        self.pluginDelegate = plgn
        self.command = command
    }

    func goAway (){
        let docsResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "El Usuario ha cancelado")
        self.pluginDelegate.commandDelegate.send(docsResult, callbackId: self.command.callbackId)
        self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }

    func enableCamSwitcher(){
        self.view.addSubview(changeCamButton)
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.qweqwe(notification:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

        setupCameraSession()


        cropperSquare = ViewFinderView(frame: CGRect(x: 0, y: 0, width: screenSize.width - 30, height: (screenSize.width - 30) * 0.63))
        cropperSquare.frame = CGRect(x: 0, y: 0, width: screenSize.width - 30, height: (screenSize.width - 30) * 0.63)
//        cropperSquare.layer.borderWidth = 6.0
//        cropperSquare.layer.borderColor = activeBlue.cgColor
        cropperSquare.center = self.view.center
        cropperSquare.backgroundColor = UIColor.clear

        shutterButton.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        shutterView.frame = CGRect(x: 0, y: screenSize.height - 80, width: screenSize.width, height: 80)
        shutterButton.center.x = self.shutterView.center.x
        shutterButton.setImage(UIImage(named: "camara"), for: .normal)
        shutterView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)

        let pincher = UIPinchGestureRecognizer(target: self, action: #selector(userDidPinch(sender:)))
        let panner = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))

        self.shutterButton.addTarget(self, action: #selector(takePic), for: .touchUpInside)

        self.rotatePortrait()

        self.view.addGestureRecognizer(pincher)
        self.view.addGestureRecognizer(panner)

        imageContainer.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        imageContainer.backgroundColor = UIColor.clear


        self.title = "Documento X"
        self.navigationController?.navigationBar.barTintColor = allianzeBlue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Reconocimiento Facial"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent


        changeCamButton.frame = CGRect(x: 10, y:  100, width: 45, height: 45)
        changeCamButton.setImage(UIImage(named: "switchCam"), for: .normal)
        changeCamButton.setTitleColor(UIColor.white, for: .normal)
        changeCamButton.tintColor = UIColor.white
        changeCamButton.addTarget(self, action: #selector(updateCameraSelection), for: .touchUpInside)
        changeCamButton.backgroundColor = UIColor.clear



        messageLabel.text = "Esta es una Instrucción"
        messageLabel.frame = CGRect(x: 0, y: 64, width: screenSize.height * 2, height: 30)
        messageLabel.textColor = UIColor.white
        messageLabel.textAlignment = .center
        messageLabel.center.x = self.view.center.x
        messageLabel.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)

        rightMessageLabel.text = "REsta es una Instrucción"
        rightMessageLabel.frame = CGRect(x: -100, y: 0, width: 250, height: 60)
        rightMessageLabel.numberOfLines = 2
        rightMessageLabel.textColor = UIColor.white
        rightMessageLabel.textAlignment = .center
        rightMessageLabel.center.y = self.view.center.y
        rightMessageLabel.layer.shadowColor = UIColor.black.cgColor
        rightMessageLabel.layer.shadowOffset = CGSize(width: 5, height: 5)
        rightMessageLabel.layer.shadowRadius = 1
        rightMessageLabel.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))

        leftMessageLabel.text = "LEsta es una Instrucción"
        leftMessageLabel.frame = CGRect(x: screenSize.width + 100 - 250, y: 0, width: 250, height: 60)
        leftMessageLabel.numberOfLines = 2
        leftMessageLabel.textColor = UIColor.white
        leftMessageLabel.textAlignment = .center
        leftMessageLabel.center.y = self.view.center.y
        leftMessageLabel.layer.shadowColor = UIColor.black.cgColor
        leftMessageLabel.layer.shadowOffset = CGSize(width: 5, height: 5)
        leftMessageLabel.layer.shadowRadius = 1
        leftMessageLabel.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))

        topMask.frame = CGRect(x: 0, y: messageLabel.frame.origin.y + messageLabel.frame.height, width: screenSize.width, height:((cropperSquare.frame.origin.y)-(messageLabel.frame.origin.y + messageLabel.frame.height)))
        bottomMask.frame = CGRect(x: 0, y: (cropperSquare.frame.origin.y + cropperSquare.frame.height), width: screenSize.width, height: ((shutterView.frame.origin.y) - ((cropperSquare.frame.origin.y + cropperSquare.frame.height))))
        leftMask.frame = CGRect(x: 0, y: (topMask.frame.origin.y + topMask.frame.height), width: (cropperSquare.frame.origin.x), height: ((bottomMask.frame.origin.y) - (topMask.frame.origin.y + topMask.frame.height)))
        rightMask.frame = CGRect(x: cropperSquare.frame.origin.x + cropperSquare.frame.width, y: (topMask.frame.origin.y + topMask.frame.height), width: (cropperSquare.frame.origin.x), height: ((bottomMask.frame.origin.y) - (topMask.frame.origin.y + topMask.frame.height)))

        topMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        leftMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        rightMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        bottomMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)

        curtain.alpha = 0
        curtain.backgroundColor = UIColor.black

        adjust()

        currentDeviceOrientation = UIDevice.current.orientation

        lLMessage.frame = CGRect(x: 10, y: self.shutterView.frame.origin.y, width: 60, height: self.shutterView.frame.height)
        rLMessage.frame = CGRect(x: screenSize.width - 70, y: self.shutterView.frame.origin.y, width: 60, height: self.shutterView.frame.height)
        lLMessage.setTitleColor(UIColor.white, for: .normal)
        rLMessage.setTitleColor(UIColor.white, for: .normal)
        lLMessage.setTitle("Repetir", for: .normal)
        rLMessage.setTitle("Listo", for: .normal)

        lLMessage.addTarget(self, action: #selector(cameraMode), for: .touchUpInside)
        rLMessage.addTarget(self, action: #selector(croppActionForButton), for: .touchUpInside)


        intermidiateView.frame = self.view.frame
        intermidiateView.backgroundColor = UIColor.clear

        self.view.layer.addSublayer(previewLayer);      self.view.addSubview(curtain)
        self.view.addSubview(imageContainer);           self.view.addSubview(intermidiateView)
        self.view.addSubview(cropperSquare);
        self.view.addSubview(shutterView);              self.shutterView.addSubview(shutterButton)
        self.view.addSubview(topMask);                  self.view.addSubview(rightMask)
        self.view.addSubview(leftMask);                 self.view.addSubview(bottomMask)
        self.view.addSubview(messageLabel);             self.view.addSubview(rightMessageLabel)
        self.view.addSubview(leftMessageLabel);         self.view.addSubview(lLMessage)
        self.view.addSubview(rLMessage);


        let navbar = UINavigationBar(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width,height:65));
        navbar.tintColor = UIColor.white
        navbar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navbar.barTintColor = allianzeBlue

        self.view.addSubview(navbar)

        let navItem = UINavigationItem(title: "Credencial")

        let navBarbutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(goAway))
        navItem.leftBarButtonItem = navBarbutton


        navbar.items = [navItem]


    }

    func qweqwe(notification: NSNotification){
        if !OnEditingMode {
            let currentOrientation = UIDevice.current.orientation
            print("Orientation Changed")
            // Ignore changes in device orientation if unknown, face up, or face down.
            if !UIDeviceOrientationIsValidInterfaceOrientation(currentOrientation) {
                return;
            }
            switch currentOrientation {
            case .landscapeLeft:
                self.rotateRight()
            case .landscapeRight:
                self.rotateLeft()
            case .portrait:
                self.rotatePortrait()
            default:
                print("notValidOrientation")
            }

        }
    }

    func cameraMode(){
        messageLabel.text = "Toma la foto encuadrando el documento"
        leftMessageLabel.text = "Toma la foto encuadrando el documento"
        rightMessageLabel.text = "Toma la foto encuadrando el documento"
        OnEditingMode = false
        cameraSession.startRunning()
        lastCenter = CGPoint(x: screenSize.width / 2, y: screenSize.height / 2)
        UIView.animate(withDuration: 0.3) {
            self.cropperSquare.cameraMode()
            self.topMask.alpha = 0;         self.bottomMask.alpha = 0
            self.leftMask.alpha = 0;        self.rightMask.alpha = 0
            self.lLMessage.alpha = 0;       self.rLMessage.alpha = 0
            self.curtain.alpha = 0;         self.shutterButton.alpha = 1
            self.imageContainer.alpha = 0
            self.lLMessage.isUserInteractionEnabled = false
            self.rLMessage.isUserInteractionEnabled = false
            self.shutterButton.isUserInteractionEnabled = true
        }
    }

    func viewEditMode(){
        messageLabel.text = "Ajusta la imagen"
        leftMessageLabel.text = "Ajusta la imagen"
        rightMessageLabel.text = "Ajusta la imagen"
        OnEditingMode = true
        cameraSession.stopRunning()
        UIView.animate(withDuration: 0.3) {
            self.cropperSquare.ViewEditMode()
            self.topMask.alpha = 1;         self.bottomMask.alpha = 1
            self.leftMask.alpha = 1;        self.rightMask.alpha = 1
            self.lLMessage.alpha = 1;       self.rLMessage.alpha = 1
            self.curtain.alpha = 1;         self.shutterButton.alpha = 0
            self.imageContainer.alpha = 1
            self.lLMessage.isUserInteractionEnabled = true
            self.rLMessage.isUserInteractionEnabled = true
            self.shutterButton.isUserInteractionEnabled = false
        }
    }

    func rotateRight(){
        UIView.animate(withDuration: 0.4) {
            self.shutterButton.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
            self.cropperSquare.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
//            self.imageContainer.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
            self.lLMessage.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
            self.rLMessage.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
            self.messageLabel.transform = CGAffineTransform.init(translationX: 0, y: -30)
            self.adjust()
            self.leftMessageLabel.alpha = 1
            self.rightMessageLabel.alpha = 0

        }

    }

    func rotateLeft(){
        UIView.animate(withDuration: 0.4) {
            self.shutterButton.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
            self.cropperSquare.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
//            self.imageContainer.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
            self.lLMessage.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
            self.rLMessage.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
            self.messageLabel.transform = CGAffineTransform.init(translationX: 0, y: -30)
            self.adjust()
            self.leftMessageLabel.alpha = 0
            self.rightMessageLabel.alpha = 1

        }

    }

    func rotatePortrait(){
        UIView.animate(withDuration: 0.4) {
            self.shutterButton.transform = CGAffineTransform.identity
            self.cropperSquare.transform = CGAffineTransform.identity
            self.messageLabel.transform = CGAffineTransform.identity
            self.lLMessage.transform = CGAffineTransform.identity
            self.rLMessage.transform = CGAffineTransform.identity
            self.imageContainer.transform = CGAffineTransform.init(rotationAngle: CGFloat(0))
            self.adjust()
            self.leftMessageLabel.alpha = 0
            self.rightMessageLabel.alpha = 0
        }
    }

    func adjust(){
        topMask.frame = CGRect(x: 0, y: messageLabel.frame.origin.y + messageLabel.frame.height, width: screenSize.width, height:((cropperSquare.frame.origin.y)-(messageLabel.frame.origin.y + messageLabel.frame.height)))
        bottomMask.frame = CGRect(x: 0, y: (cropperSquare.frame.origin.y + cropperSquare.frame.height), width: screenSize.width, height: ((shutterView.frame.origin.y) - ((cropperSquare.frame.origin.y + cropperSquare.frame.height))))
        leftMask.frame = CGRect(x: 0, y: (topMask.frame.origin.y + topMask.frame.height), width: (cropperSquare.frame.origin.x), height: ((bottomMask.frame.origin.y) - (topMask.frame.origin.y + topMask.frame.height)))
        rightMask.frame = CGRect(x: cropperSquare.frame.origin.x + cropperSquare.frame.width, y: (topMask.frame.origin.y + topMask.frame.height), width: (cropperSquare.frame.origin.x), height: ((bottomMask.frame.origin.y) - (topMask.frame.origin.y + topMask.frame.height)))

        topMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        leftMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        rightMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        bottomMask.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
    }
    var lastScale: CGFloat = 1
    func userDidPinch(sender: UIPinchGestureRecognizer){

        print(sender.scale)
        print("LS\(lastScale)")
        let newScale = self.lastScale + (sender.scale - 1)
        self.imageContainer.transform = (CGAffineTransform.init(scaleX: newScale, y: newScale))
        if sender.state == .ended {
            lastScale = newScale
        }


    }

    func screenShotMethod(forView: UIView)-> UIImage {
        //Create the UIImage
        UIGraphicsBeginImageContext(forView.frame.size)
        forView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //Save it to the camera roll
        return image!
    }
    var lastCenter: CGPoint! = CGPoint(x: screenSize.width / 2, y: screenSize.height / 2)
    func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
//        lastCenter = imageContainer.center
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {

            let translation = gestureRecognizer.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped
            print("COMPARING\(gestureRecognizer.view!.center)::: \(self.lastCenter!)")
            imageContainer.center = CGPoint(x: lastCenter.x + translation.x, y: lastCenter.y + translation.y)

        } else if gestureRecognizer.state == .ended {
            self.lastCenter = imageContainer.center
        }
    }

    let curtain = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.cameraMode()



    }

    func takePic(){
        capturePicture()
        self.viewEditMode()

    }
    var backCameraTurnedOn = true

    func updateCameraSelection(){

        if self.cameraSession.inputs.count > 0 {
            for i in 0...(self.cameraSession.inputs.count - 1){
                self.cameraSession.removeInput(self.cameraSession.inputs[i] as! AVCaptureInput)
            }

        }

        var desiredPosition: AVCaptureDevicePosition!

        if self.backCameraTurnedOn {
            desiredPosition = AVCaptureDevicePosition.front
            self.backCameraTurnedOn = false
        } else {
            desiredPosition = AVCaptureDevicePosition.back
            self.backCameraTurnedOn = true
        }


        let input = cameraForPosition(desiredPosition: desiredPosition)
        if !(input != nil) {

        } else {
            self.cameraSession.addInput(input)
        }
        self.cameraSession.commitConfiguration()
    }


    func cameraForPosition(desiredPosition: AVCaptureDevicePosition) -> AVCaptureDeviceInput? {
        let hadError = false
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as! [AVCaptureDevice] {
            var error: NSError? = nil
            do {
                if device.position == desiredPosition {
                    var input = try AVCaptureDeviceInput(device: device)
                    if self.cameraSession.canAddInput(input) {
                        return input
                    }

                }
            }
            catch {
                print("Somehting went wrong \(error)" )
            }
        }
        return nil
    }


    func croppActionForButton(){
        cropToBounds(screenshot: self.imageContainer.image!, width: Double(cropperSquare.frame.width), height: Double(cropperSquare.frame.width))
    }

    lazy var cameraSession: AVCaptureSession = {
        let s = AVCaptureSession()
        s.sessionPreset = AVCaptureSessionPresetHigh
        return s
    }()

    lazy var previewLayer: AVCaptureVideoPreviewLayer = {
        let preview =  AVCaptureVideoPreviewLayer(session: self.cameraSession)
        preview?.bounds = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        preview?.position = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        preview?.videoGravity = AVLayerVideoGravityResize
        return preview!
    }()

    func setupCameraSession() {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) as AVCaptureDevice

            do {
                let deviceInput = try AVCaptureDeviceInput(device: captureDevice)

                self.cameraSession.beginConfiguration()

                if (self.cameraSession.canAddInput(deviceInput) == true) {
                    self.cameraSession.addInput(deviceInput)
                }

                self.cameraSession.commitConfiguration()

                //            let queue = DispatchQueue(label: "com.videoQueue")
                self.cameraSession.addOutput(self.stillImageOutput)

                if (self.stillImageOutput.connection(withMediaType: AVMediaTypeVideo)) != nil{




                }

            }
            catch let error as NSError {
                NSLog("\(error), \(error.localizedDescription)")
            }

        }
    }



    func capturePicture(){

        print("Capturing image")

        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160]
        settings.previewPhotoFormat = previewFormat
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }




    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?){
            if let error = error {
                print(error.localizedDescription)
            }
            if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
                print("image: \(UIImage(data: dataImage)?.size)")
                DispatchQueue.main.async {
                    self.imageContainer.image = UIImage(data: dataImage)
                }
            }

    }

    func cropToBounds(screenshot: UIImage, width: Double, height: Double)  {

        cropperSquare.contentMode = .scaleAspectFit

//        CALayer *layer;
//        layer = self.view.layer;
//        UIGraphicsBeginImageContext(self.view.frame.size);
//        CGContextClipToRect (UIGraphicsGetCurrentContext(),captureFrame);
//        [layer renderInContext:UIGraphicsGetCurrentContext()];
//        UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        return screenImage;

        cropperSquare.alpha = 0

        var layer: CALayer
        layer = self.view.layer
        UIGraphicsBeginImageContext(self.intermidiateView.frame.size)

        let context = UIGraphicsGetCurrentContext()
        context?.clip(to: cropperSquare.frame)
        layer.render(in: context!)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        cropperSquare.alpha = 1


        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData

        self.result = imageData.base64EncodedString(options: .lineLength64Characters)
        let docsResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: self.result)
            self.pluginDelegate.commandDelegate.send(docsResult, callbackId: self.command.callbackId)
            self.dismiss(animated: true, completion: nil)

    }


    func cropImage(screenshot: UIImage) -> UIImage {
        let crop = CGRect(x:cropperSquare.frame.origin.y, y:cropperSquare.frame.origin.x,width: //"start" at the upper-left corner
            cropperSquare.frame.height ,height: //include half the width of the whole screen
           cropperSquare.frame.width ) //include the height of the navigationBar and the height of view

        let cgImage = screenshot.cgImage!.cropping(to: crop)
        let image: UIImage = UIImage(cgImage: cgImage!)
        return image
    }


}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!)
    }
}
