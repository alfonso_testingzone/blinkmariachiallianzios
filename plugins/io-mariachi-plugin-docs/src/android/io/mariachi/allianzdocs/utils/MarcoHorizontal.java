package io.mariachi.allianzdocs.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


/**
 * Created by Ken on 14/03/17.
 */

public class MarcoHorizontal extends View {
    private Context context;

    public static final float CONSTANT_X = 5.8f;
    public static final float CONSTANT_Y = 9.5f;


    public MarcoHorizontal(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Dividimos nuestro canvas en 10 partes
        int casilla = canvas.getHeight() / 10;

        /**
         * Definimos cual es el
         */
        int recHeight = (int) (casilla * CONSTANT_X);
        int recWidth = (int) (casilla * CONSTANT_Y);


        int espacioX = ((canvas.getWidth() - recWidth) / 2);
        int espacioY = ((canvas.getHeight() - recHeight) / 2);

        int barrax = recWidth / 3;
        int barray = recHeight / 3;

        Utils.x = barrax;
        Utils.y = barray;
        Utils.alto = recHeight;
        Utils.ancho = recWidth;
        Utils.rejilla = 10;
        Utils.constanteX = CONSTANT_X;
        Utils.constanteY = CONSTANT_Y;

        Paint myGrayPaint = new Paint();
        myGrayPaint.setColor(getResources().getColor(getResources().getIdentifier("black", "color", context.getPackageName())));
        
        myGrayPaint.setAlpha(0);

        Paint myPaint = new Paint();
        myPaint.setColor(getResources().getColor(getResources().getIdentifier("verde", "color", context.getPackageName())));
        myPaint.setStrokeWidth(15);

        canvas.drawPoint(espacioX, espacioY, myPaint); //punto superior izquierdo
        canvas.drawPoint(recWidth + espacioX, espacioY, myPaint); //punto superior derecho
        canvas.drawPoint(espacioX, recHeight + espacioY, myPaint); //punto inferior izquierdo
        canvas.drawPoint(recWidth + espacioX, espacioY + recHeight, myPaint); //punto inferior derecho


        canvas.drawRect(0, 0, canvas.getWidth(), espacioY, myGrayPaint);//top
        canvas.drawRect(0, espacioY, espacioX, canvas.getHeight(), myGrayPaint);//left
        canvas.drawRect(espacioX, recHeight + espacioY, canvas.getWidth(), canvas.getHeight(), myGrayPaint); //bottom
        canvas.drawRect(recWidth + espacioX, espacioY, canvas.getWidth(), recHeight + espacioY, myGrayPaint); //right


        canvas.drawLine(espacioX, espacioY, espacioX + (barrax / 2), espacioY, myPaint);
        canvas.drawLine((recWidth + espacioX) - (barrax / 2), espacioY, recWidth + espacioX, espacioY, myPaint);

        canvas.drawLine(espacioX, recHeight + espacioY, espacioX + (barrax / 2), espacioY + recHeight, myPaint);
        canvas.drawLine((recWidth + espacioX) - (barrax / 2), recHeight + espacioY, recWidth + espacioX, espacioY + recHeight, myPaint);

        canvas.drawLine(espacioX, espacioY, espacioX, espacioY + (barrax / 2), myPaint);
        canvas.drawLine(espacioX, (recHeight + espacioY) - (barrax / 2), espacioX, recHeight + espacioY, myPaint);

        canvas.drawLine(recWidth + espacioX, espacioY, recWidth + espacioX, espacioY + (barrax / 2), myPaint);
        canvas.drawLine(recWidth + espacioX, (recHeight + espacioY) - (barrax / 2), recWidth + espacioX, espacioY + recHeight, myPaint);


//        canvas.drawRect(0, 0, canvas.getWidth(), espacioy, myGrayPaint);//top
//        canvas.drawRect(0, espacioy, espaciox, canvas.getHeight(), myGrayPaint);//left
//        canvas.drawRect(espaciox, alto + espacioy, canvas.getWidth(), canvas.getHeight(), myGrayPaint); //bottom
//        canvas.drawRect(ancho + espaciox, espacioy, canvas.getWidth(), alto + espacioy, myGrayPaint); //right


    }


}
