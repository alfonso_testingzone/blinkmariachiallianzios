package io.mariachi.allianzdocs.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.mariachi.allianzdocs.utils.Utils;

public class Cards extends AppCompatActivity {
    private static final int MY_SCAN_REQUEST_CODE = 37;
    JSONObject cardObj = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources().getIdentifier("activity_cards", "layout", getPackageName()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent scanIntent = new Intent(this, CardIOActivity.class);

        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO,true);

        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                cardObj = new JSONObject();
                try {
                    cardObj.put("card_number",scanResult.cardNumber);
                    cardObj.put("card_type",scanResult.getCardType().getDisplayName("en-US"));
                    cardObj.put("redacted_card_number", scanResult.getFormattedCardNumber());
                    if (scanResult.isExpiryValid()) {
                        cardObj.put("expiry_month",scanResult.expiryMonth);
                        cardObj.put("expiry_year",scanResult.expiryYear);
                    }

                    if (scanResult.cvv != null) {
                        cardObj.put("cvv",scanResult.cvv);

                    }

                    if (scanResult.postalCode != null) {
                        cardObj.put("zip",scanResult.postalCode);
                    }
                    Utils.flagCard = true;
                    Utils.cardInfo = cardObj;
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                finish();
            }
        }
    }
}
