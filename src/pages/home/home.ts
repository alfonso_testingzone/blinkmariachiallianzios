import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

declare var blinkplugin : any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public base64Image: string;

  constructor(public navCtrl: NavController) {

  }

  blinkFunction(){

    console.log("On blink");

    blinkplugin.blinkMethod('', funcionOk, funcionError);

    function funcionOk(response){

          console.log("Function ok ");

          //var img = "data:image/jpg;base64,"+response;

          alert("response success "+response);

          this.base64Image = "data:image/jpeg;base64," + response;
          //document.getElementById("blinkImg").src = img;
      }

      function funcionError(response){
          //TODO aquí muestra algún error como evento cancelado
          console.log("Function error");
      }

  }

}
