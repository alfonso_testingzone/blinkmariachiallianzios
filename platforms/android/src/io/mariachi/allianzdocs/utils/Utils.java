package io.mariachi.allianzdocs.utils;

import org.json.JSONObject;

/**
 * Created by antonio .
 */

public class Utils {
    public static int x = 0;
    public static int y = 0;
    public static int ancho = 0;
    public static int alto = 0;
    public static int rejilla = 0;
    public static float constanteX = 0;
    public static float constanteY = 0;

    public static byte[] pictureFinal = null;
    public static byte[] pictureTaken = null;
    public static boolean itsFinal = false;

    public static boolean blink = false;

    public static boolean flagCard = false;
    public static JSONObject cardInfo = null;
}
