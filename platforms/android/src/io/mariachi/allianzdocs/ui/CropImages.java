package io.mariachi.allianzdocs.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;

import io.mariachi.allianzdocs.MainActivity;
import io.mariachi.allianzdocs.utils.Utils;

public class CropImages extends AppCompatActivity {
    CropImageView cropImageView;
    ImageView apply;
    ImageView btnBack;
    ImageView retake;
    ImageView imgRotate;
    ImageView prev;
    private Bitmap realImage;

    private int rotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources().getIdentifier("activity_crop_images", "layout", getPackageName()));
        Log.e("myLog", "init crop");
        btnBack = (ImageView) findViewById(getResources().getIdentifier("backCrop", "id", getPackageName()));
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        retake = (ImageView) findViewById(getResources().getIdentifier("btnRetakeCrop", "id", getPackageName()));
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();


            }
        });
        apply = (ImageView) findViewById(getResources().getIdentifier("btnCropOk", "id", getPackageName()));
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyCrop();

            }
        });
        imgRotate = (ImageView) findViewById(getResources().getIdentifier("img_rotate", "id", getPackageName()));

        imgRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cropImageView.rotateImage(90);
            }
        });


        byte[] imgArray = Utils.pictureTaken;

        realImage = BitmapFactory.decodeByteArray(imgArray, 0, imgArray.length);

        cropImageView = (CropImageView) findViewById(getResources().getIdentifier("cropImageView", "id", getPackageName()));
        cropImageView.setImageBitmap(realImage);
        cropImageView.setAutoZoomEnabled(false);

        cropImageView.setGuidelines(CropImageView.Guidelines.ON);
        cropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
        cropImageView.setAutoZoomEnabled(true);

    }

    public void applyCrop() {
        Utils.itsFinal = true;

        cropImageView.setOnCropImageCompleteListener(new CropImageView.OnCropImageCompleteListener() {
            @Override
            public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
                Bitmap bitmap = result.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                byte[] byteArray = stream.toByteArray();

                Utils.pictureFinal = byteArray;
                Utils.pictureTaken = null;
                finish();
            }
        });

        cropImageView.getCroppedImageAsync();

    }

    @Override
    protected void onStop() {
        super.onStop();
        cropImageView.setOnCropImageCompleteListener(null);
    }
}
