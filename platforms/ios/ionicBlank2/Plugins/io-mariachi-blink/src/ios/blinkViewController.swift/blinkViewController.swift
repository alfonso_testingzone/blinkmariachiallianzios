//
//  ViewController.swift
//  AllianzBlink
//
//  Created by Rodolfo Castillo on 3/15/17.
//  Copyright © 2017 Mariachi.io. All rights reserved.
//

import AVFoundation
import UIKit
import GoogleMobileVision
import GoogleMVDataOutput

let screenSize: CGRect = UIScreen.main.bounds
let allianzeBlue = UIColor(red: 95/255, green: 132/255, blue: 199/255, alpha: 1)
let activeBlue = UIColor(red: 33/255, green: 156/255, blue: 232/255, alpha: 1)


class blinkViewController: UIViewController, AVCapturePhotoCaptureDelegate, GMVMultiDataOutputDelegate, FaceTrackerDataSource {


    var pluginDelegate: BlinkPlugin!
    var command: CDVInvokedUrlCommand!

    fileprivate var _orientations = UIInterfaceOrientationMask.portrait
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        get { return self._orientations }
        set { self._orientations = newValue }
    }



    var placeHolder: UIView = UIView(frame: screenSize)
    var overlay: UIView = UIView(frame: screenSize)

    lazy var previewLayerII: AVCaptureVideoPreviewLayer = {
        let preview =  AVCaptureVideoPreviewLayer(session: self.session)
        preview?.bounds = CGRect(x: 0 - ((screenSize.height - screenSize.width)/2), y: 20, width: screenSize.height, height: screenSize.height)
        preview?.position = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)

        preview?.videoGravity = AVLayerVideoGravityResizeAspect
        return preview!
    }()

    var session: AVCaptureSession!
    var dataOutput: GMVDataOutput!
    var previewLayer: AVCaptureVideoPreviewLayer!

    var time_aux = 0
    var timerAux: Timer!

    var blinkTimer: Timer!

    let stages = ["StandBY": "Toma la foto", "Armed": "Asegurate que parpadee, al finalizar la cuenta regresiva", "Activated": "Debe Parpadear al finalizar la cuenta regresiva", "Wrong": "Algo Salio mal"]
    var auxStage = 0

    var currentDeviceOrientation: UIDeviceOrientation = .unknown

    var changeCamButton = UIButton(type: .system)
    let shutterView = UIView()
    let shutterButton = UIButton()
    let activeButton = UIButton()
    let messageLabel = UILabel()
    let rightMessageLabel = UILabel()
    let leftMessageLabel = UILabel()
    let bottomMessageLabel = UILabel()
    let videoScanView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
    let circleView = UIImageView()
    let circleLabel = UILabel()

    func setPluginDelegate(forPluggin plgn: BlinkPlugin, andCommand command: CDVInvokedUrlCommand){
        self.pluginDelegate = plgn
        self.command = command
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.session = AVCaptureSession()
        self.session.sessionPreset = AVCaptureSessionPresetMedium

        self.view.layer.addSublayer(previewLayerII)

        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.qweqwe(notification:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)

        self.shutterButton.addTarget(self, action: #selector(armTrigger), for: .touchUpInside)

        self.navigationController?.navigationBar.barTintColor = allianzeBlue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Reconocimiento Facial"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UIApplication.shared.statusBarStyle = .lightContent



        currentDeviceOrientation = UIDevice.current.orientation

        self.updateCameraSelection()

        self.setupGMVDataOutput()

        self.setupCameraPreview()

        self.view.addSubview(placeHolder)
        self.view.addSubview(overlay)




        let navbar = UINavigationBar(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width,height:65));
        navbar.tintColor = UIColor.white
        navbar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navbar.barTintColor = allianzeBlue

        self.view.addSubview(navbar)

        let navItem = UINavigationItem(title: "Reconozimiento facial")

        let navBarbutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(goAway))
        navItem.leftBarButtonItem = navBarbutton


        navbar.items = [navItem]

    }

    var face: FaceTracker!

    var blinkAux = 0
    var tryAux: Double = 0

    func blinkHandler(){

        self.tryAux += 0.3
        print(self.tryAux)
//        print("DEBUGGER: Buscando Parpadeo...")
        if tryAux >= 10 {
            //FAILED
            self.messageLabel.text = stages["Wrong"]
            //        self.bottomMessageLabel.text = stages["StandBY"]
            self.leftMessageLabel.text = stages["Wrong"]
            self.rightMessageLabel.text = stages["Wrong"]
            self.circleLabel.text = ""

            self.blinkTimer.invalidate()
            print("Se agoto el tiempo")
            self.tryAux = 0

        }
        if face.userClosedEyes() {
            blinkAux += 1
            print("Parpadeo! \(blinkAux)")
            let blinkresult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: self.result)
            self.pluginDelegate.commandDelegate.send(blinkresult, callbackId: self.command.callbackId)
            self.dismiss(animated: true, completion: nil)
        }
    }



    override func viewDidAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.session.startRunning()

        messageLabel.text = "Esta es una Instrucción"
        messageLabel.frame = CGRect(x: 0, y: 64, width: screenSize.height * 2, height: 30)
        messageLabel.textColor = UIColor.white
        messageLabel.textAlignment = .center
        messageLabel.center.x = self.view.center.x
        messageLabel.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)

        rightMessageLabel.text = "REsta es una Instrucción"
        rightMessageLabel.frame = CGRect(x: -100, y: 0, width: 250, height: 60)
        rightMessageLabel.numberOfLines = 2
        rightMessageLabel.textColor = UIColor.white
        rightMessageLabel.textAlignment = .center
        rightMessageLabel.center.y = self.view.center.y
        rightMessageLabel.layer.shadowColor = UIColor.black.cgColor
        rightMessageLabel.layer.shadowOffset = CGSize(width: 5, height: 5)
        rightMessageLabel.layer.shadowRadius = 1
        rightMessageLabel.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))

        leftMessageLabel.text = "LEsta es una Instrucción"
        leftMessageLabel.frame = CGRect(x: screenSize.width + 100 - 250, y: 0, width: 250, height: 60)
        leftMessageLabel.numberOfLines = 2
        leftMessageLabel.textColor = UIColor.white
        leftMessageLabel.textAlignment = .center
        leftMessageLabel.center.y = self.view.center.y
        leftMessageLabel.layer.shadowColor = UIColor.black.cgColor
        leftMessageLabel.layer.shadowOffset = CGSize(width: 5, height: 5)
        leftMessageLabel.layer.shadowRadius = 1
        leftMessageLabel.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))



        shutterButton.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        shutterView.frame = CGRect(x: 0, y: screenSize.height - 80, width: screenSize.width, height: 80)
        shutterButton.center.x = self.shutterView.center.x
        shutterButton.setImage(UIImage(named: "camera"), for: .normal)
        shutterView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        shutterView.autoresizingMask = .flexibleBottomMargin

        activeButton.frame = shutterView.frame
        activeButton.backgroundColor = activeBlue
        activeButton.setTitleColor(UIColor.white, for: .normal)
        activeButton.alpha = 0
        activeButton.setTitle("Comenzar", for: .normal)

        bottomMessageLabel.frame = CGRect(x: 0, y: activeButton.frame.origin.y - 70, width: screenSize.width, height: 60)
        bottomMessageLabel.textColor = UIColor.white
        bottomMessageLabel.textAlignment = .center
        bottomMessageLabel.numberOfLines = 2

        leftMessageLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        leftMessageLabel.layer.shadowOpacity = 1
        leftMessageLabel.layer.shadowRadius = 6
        rightMessageLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        rightMessageLabel.layer.shadowOpacity = 1
        rightMessageLabel.layer.shadowRadius = 6
        bottomMessageLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        bottomMessageLabel.layer.shadowOpacity = 1
        bottomMessageLabel.layer.shadowRadius = 6

        circleView.frame = CGRect(x: 0, y: 0, width: screenSize.width - 30, height: screenSize.width - 30)
        circleView.clipsToBounds = true
        circleView.layer.cornerRadius = circleView.frame.width / 2
        circleView.center = self.view.center
        circleView.image = UIImage(named: "cross")

        circleLabel.frame = CGRect(x: 0, y: 0, width: screenSize.width - 60, height: screenSize.width - 60)
        circleLabel.font = UIFont(name: "Helvetica", size: 58)
        circleLabel.textColor = UIColor.white
        circleLabel.textAlignment = .center
        circleLabel.center = self.view.center

        self.messageLabel.text = stages["StandBY"]
//        self.bottomMessageLabel.text = stages["StandBY"]
        self.leftMessageLabel.text = stages["StandBY"]
        self.rightMessageLabel.text = stages["StandBY"]

        self.activeButton.addTarget(self, action: #selector(startProcess), for: .touchUpInside)

        self.rightMessageLabel.alpha = 0
        self.leftMessageLabel.alpha = 0



        changeCamButton.frame = CGRect(x: 10, y:  self.messageLabel.frame.origin.y + 35, width: 45, height: 45)
        changeCamButton.setImage(UIImage(named: "switchCam"), for: .normal)
        changeCamButton.setTitleColor(UIColor.white, for: .normal)
        changeCamButton.tintColor = UIColor.white
        changeCamButton.addTarget(self, action: #selector(updateCameraSelection), for: .touchUpInside)
        changeCamButton.backgroundColor = UIColor.clear

        self.view.addSubview(shutterView)
        self.view.bringSubview(toFront: shutterView)
        self.shutterView.addSubview(shutterButton)
        self.view.addSubview(messageLabel)
        self.view.addSubview(leftMessageLabel)
        self.view.addSubview(rightMessageLabel)
        self.view.addSubview(activeButton)
        self.view.addSubview(bottomMessageLabel)
        self.view.addSubview(circleView)
        self.view.addSubview(circleLabel)
        self.view.addSubview(changeCamButton)

        print("\(previewLayer.frame.height) = \(screenSize.height)")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.session.stopRunning()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var stillImageOutput = AVCapturePhotoOutput()

    func capturePicture(){

        print("Capturing image")

        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160]
        settings.previewPhotoFormat = previewFormat
        stillImageOutput.capturePhoto(with: settings, delegate: self)


    }


    func armTrigger(){
        self.capturePicture()
        self.messageLabel.text = ""
        self.messageLabel.backgroundColor = UIColor.clear
        self.bottomMessageLabel.text = stages["Armed"]
        self.leftMessageLabel.text = stages["Armed"]
        self.rightMessageLabel.text = stages["Armed"]
        self.view.bringSubview(toFront: activeButton)
        UIView.animate(withDuration: 0.4) {
            self.activeButton.alpha = 1
            self.shutterButton.alpha = 0
        }
    }

    func startProcess (){
        print("Initializing Timer...")
        self.timerAux = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimerForProcess), userInfo: nil, repeats: true)

    }

    func updateTimerForProcess(){

        if self.time_aux < 3 {
            print("Time till process \(3 - self.time_aux)")
            self.circleLabel.text = "\(3 - self.time_aux)"
        } else {

            print("Running SVM Algorithm")
            self.circleLabel.text = "Parpadea"
            self.timerAux.invalidate()
            self.blinkTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(blinkHandler), userInfo: nil, repeats: true)
            self.blinkTimer.fire()
        }
        self.time_aux += 1
    }

    func qweqwe(notification: Notification) {
        let currentOrientation = UIDevice.current.orientation
        print("this changed")
        // Ignore changes in device orientation if unknown, face up, or face down.
        if !UIDeviceOrientationIsValidInterfaceOrientation(currentOrientation) {
            return;
        }

        switch currentOrientation {
        case .landscapeLeft:
            self.rotateRight()
        case .landscapeRight:
            self.rotateLeft()
        case .portrait:
            self.rotatePortrate()
        default:
            print("notValidOrientation")
        }

        // Rotate your view, or other things here
    }

    func goAway(){
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func rotateRight(){
        UIView.animate(withDuration: 0.4) {
            self.shutterButton.transform = CGAffineTransform.identity
            self.changeCamButton.transform = CGAffineTransform.identity
            self.shutterButton.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
            self.changeCamButton.transform = CGAffineTransform.init(rotationAngle: CGFloat(M_PI_2))
            self.messageLabel.alpha = 0
            self.bottomMessageLabel.alpha = 0
            self.rightMessageLabel.alpha = 0
            self.leftMessageLabel.alpha = 1
            self.circleView.transform = CGAffineTransform.identity
            self.circleView.transform = CGAffineTransform.init(translationX: -15, y: 0)
            self.circleView.transform = CGAffineTransform.init(scaleX: self.rightMessageLabel.frame.height/screenSize.width, y: self.rightMessageLabel.frame.height/screenSize.width)
        }
    }

    func rotateLeft(){
        UIView.animate(withDuration: 0.4) {
            self.shutterButton.transform = CGAffineTransform.identity
            self.changeCamButton.transform = CGAffineTransform.identity
            self.shutterButton.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
            self.changeCamButton.transform = CGAffineTransform.init(rotationAngle: CGFloat(0 - M_PI_2))
            self.messageLabel.alpha = 0
            self.bottomMessageLabel.alpha = 0
            self.rightMessageLabel.alpha = 1
            self.leftMessageLabel.alpha = 0
            self.circleView.transform = CGAffineTransform.identity
            self.circleView.transform = CGAffineTransform.init(translationX: 15, y: 0)
            self.circleView.transform = CGAffineTransform.init(scaleX: self.rightMessageLabel.frame.height/screenSize.width, y: self.rightMessageLabel.frame.height/screenSize.width)
        }

    }
    func rotatePortrate(){
        UIView.animate(withDuration: 0.4) {
            self.shutterButton.transform = CGAffineTransform.identity
            self.changeCamButton.transform = CGAffineTransform.identity
            self.messageLabel.alpha = 1
            self.bottomMessageLabel.alpha  = 1
            self.rightMessageLabel.alpha = 0
            self.leftMessageLabel.alpha = 0
            self.circleView.transform = CGAffineTransform.identity
        }


    }

    func rotated() {

        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft{
            self.previewLayer.frame.origin = CGPoint(x: 0, y: 0 - ((screenSize.height - screenSize.width)/2))
            self.navigationController?.isNavigationBarHidden = true
            shutterView.frame = CGRect(x: screenSize.height - 80, y: 0, width: 80, height: screenSize.width)
        }
        else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight{
            self.previewLayer.frame.origin = CGPoint(x: 0, y: 0 - ((screenSize.height - screenSize.width)/2))
            self.navigationController?.isNavigationBarHidden = true
            shutterView.frame = CGRect(x: screenSize.height - 80, y: 0, width: 80, height: screenSize.width)

        }
        else if UIDevice.current.orientation == UIDeviceOrientation.portrait{
            self.previewLayer.frame.origin = CGPoint(x: 0 - ((screenSize.height - screenSize.width)/2), y: 0)
            shutterView.frame = CGRect(x: 0, y: screenSize.height - 80, width: screenSize.width, height: 80)

            self.navigationController?.isNavigationBarHidden = false

        }
        print("YAXIS")
        print(previewLayer.frame.origin.y)
        print("XAXIS")
        print(previewLayer.frame.origin.x)

    }

    func setupGMVDataOutput(){
        let options: [AnyHashable: Any] = [GMVDetectorFaceTrackingEnabled : true,
                                           GMVDetectorFaceMode : NSInteger(200),
                                           GMVDetectorFaceLandmarkType : NSInteger(1 << 1),
                                           GMVDetectorFaceClassificationType : NSInteger(1 << 1),
                                           GMVDetectorFaceMinSize :  0.35  ]

        let detector = GMVDetector(ofType: GMVDetectorTypeFace, options: options)

        self.dataOutput = GMVMultiDataOutput(detector: detector)
        self.face = FaceTracker()


        (self.dataOutput as! GMVMultiDataOutput).multiDataDelegate = self

        if !self.session.canAddOutput(self.dataOutput) {
            return
        }
        if (self.stillImageOutput.connection(withMediaType: AVMediaTypeVideo)) != nil{


        }


        self.session.addOutput(self.stillImageOutput)
        self.session.addOutput(self.dataOutput)
    }

    func cleanupGMVDataOutput() {
        if (self.dataOutput != nil) {
            self.session.removeOutput(self.dataOutput)
        }
        self.dataOutput.cleanup()
        self.dataOutput = nil
    }

    func overlayView() -> UIView! {

        return self.overlay

    }

    func dataOutput(_ dataOutput: GMVDataOutput!, trackerFor feature: GMVFeature!) -> GMVOutputTrackerDelegate!{

        self.face = FaceTracker()

        self.face.delegate = self
        return face
    }



    func cleanUpCaptureSession(){
        self.session.stopRunning()
        self.cleanupGMVDataOutput()
        self.session = nil
        self.previewLayer.removeFromSuperlayer()
    }

    func setupCameraPreview(){
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
        self.previewLayer.frame = screenSize
        self.previewLayer.backgroundColor = UIColor.clear.cgColor
        self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        let rootLayer: CALayer = self.placeHolder.layer
        rootLayer.frame = screenSize
        rootLayer.masksToBounds = false
        //        self.previewLayer.frame = self.view.frame
        rootLayer.addSublayer(self.previewLayer)

    }

    var backCameraTurnedOn = false


    func updateCameraSelection(){

        if self.session.inputs.count > 0 {
            for i in 0...(self.session.inputs.count - 1){
                self.session.removeInput(self.session.inputs[i] as! AVCaptureInput)
            }

        }

        var desiredPosition: AVCaptureDevicePosition!

        if self.backCameraTurnedOn {
            desiredPosition = AVCaptureDevicePosition.front
            self.backCameraTurnedOn = false
        } else {
            desiredPosition = AVCaptureDevicePosition.back
            self.backCameraTurnedOn = true
        }


        let input = cameraForPosition(desiredPosition)
        if !(input != nil) {

        } else {
            self.session.addInput(input)
        }
        self.session.commitConfiguration()
    }

    func cameraForPosition(_ desiredPosition: AVCaptureDevicePosition) -> AVCaptureDeviceInput? {
        let hadError = false
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as! [AVCaptureDevice] {
            var error: NSError? = nil
            do {
                if device.position == desiredPosition {
                    let input = try AVCaptureDeviceInput(device: device)
                    if self.session.canAddInput(input) {
                        return input
                    }

                }
            }
            catch {
                print("Somehting went wrong \(error)" )
            }
        }
        return nil
    }


    var result: String = ""

    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?){
        if let error = error {
            print(error.localizedDescription)
        }
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            print("image: \(UIImage(data: dataImage)?.size)")
            DispatchQueue.main.async {
//                self.circleView.image = UIImage(data: dataImage)
            }
            let image : UIImage = UIImage(data: dataImage)!
            //Now use image to create into NSData format
            let imageData:Data = UIImagePNGRepresentation(image)! as Data

            self.result = imageData.base64EncodedString(options: .lineLength64Characters)
//            print(result)

        }

    }









}
